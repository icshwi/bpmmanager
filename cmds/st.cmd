require iocstats
require autosave
require bpmmanager

# General prefix
epicsEnvSet("P", "PBI-BPM::")

# Autosave
epicsEnvSet("AS_REMOTE", "/opt/autosave")
epicsEnvSet("AS_FOLDER", "$(P)")
epicsEnvSet("IOCDIR", "BPMManager")
iocshLoad("$(autosave_DIR)/autosave.iocsh", "AS_TOP=$(AS_REMOTE),IOCNAME=BPMManager")

############################################################################
# MEBT BPMs
############################################################################
epicsEnvSet("S1s", "MEBT")            # Section1 short name: MEBT
epicsEnvSet("S1", "$(S1s)-010")       # Section1 full name : MEBT-010
epicsEnvSet("EVR_DEV",    "EVR-101")  # EVR
epicsEnvSet("EVR_PREFIX", "PBI-BPM01:Ctrl-$(EVR_DEV):")

#######################################################
# Single BPMs 1 to 8
dbLoadRecords("$(bpmmanager_DB)/bpmmanager_single_bpm.db","P=$(P),S=$(S1),Ss=$(S1s),R=01,AMC_NUM=110,ATTN_PREFIX=Bpm1,EVR_PREFIX=$(EVR_PREFIX)")
dbLoadRecords("$(bpmmanager_DB)/bpmmanager_single_bpm.db","P=$(P),S=$(S1),Ss=$(S1s),R=02,AMC_NUM=110,ATTN_PREFIX=Bpm2,EVR_PREFIX=$(EVR_PREFIX)")
dbLoadRecords("$(bpmmanager_DB)/bpmmanager_single_bpm.db","P=$(P),S=$(S1),Ss=$(S1s),R=03,AMC_NUM=120,ATTN_PREFIX=Bpm1,EVR_PREFIX=$(EVR_PREFIX)")
## Skip number 4: dbLoadRecords("$(bpmmanager_DB)/bpmmanager_single_bpm.db","P=$(P),S=$(S1),Ss=$(S1s),R=04,AMC_NUM=120,ATTN_PREFIX=Bpm2,EVR_PREFIX=$(EVR_PREFIX)")
dbLoadRecords("$(bpmmanager_DB)/bpmmanager_single_bpm.db","P=$(P),S=$(S1),Ss=$(S1s),R=05,AMC_NUM=120,ATTN_PREFIX=Bpm2,EVR_PREFIX=$(EVR_PREFIX)")
dbLoadRecords("$(bpmmanager_DB)/bpmmanager_single_bpm.db","P=$(P),S=$(S1),Ss=$(S1s),R=06,AMC_NUM=130,ATTN_PREFIX=Bpm1,EVR_PREFIX=$(EVR_PREFIX)")
dbLoadRecords("$(bpmmanager_DB)/bpmmanager_single_bpm.db","P=$(P),S=$(S1),Ss=$(S1s),R=07,AMC_NUM=130,ATTN_PREFIX=Bpm2,EVR_PREFIX=$(EVR_PREFIX)")
dbLoadRecords("$(bpmmanager_DB)/bpmmanager_single_bpm.db","P=$(P),S=$(S1),Ss=$(S1s),R=08,AMC_NUM=140,ATTN_PREFIX=Bpm1,EVR_PREFIX=$(EVR_PREFIX)")
# TODO: DTL1 takes next AMC-140 - Bpm2

#######################################################
# Single AMCs 110 to 140 (CRATE_PREFIX:"BPM01")
dbLoadRecords("$(bpmmanager_DB)/bpmmanager_single_amc.db","P=$(P),S=$(S1),Ss=$(S1s),AMC_NUM=110,BPM_PAIR=01-02")
dbLoadRecords("$(bpmmanager_DB)/bpmmanager_single_amc.db","P=$(P),S=$(S1),Ss=$(S1s),AMC_NUM=120,BPM_PAIR=03-05")
dbLoadRecords("$(bpmmanager_DB)/bpmmanager_single_amc.db","P=$(P),S=$(S1),Ss=$(S1s),AMC_NUM=130,BPM_PAIR=06-07")
dbLoadRecords("$(bpmmanager_DB)/bpmmanager_single_amc.db","P=$(P),S=$(S1),Ss=$(S1s),AMC_NUM=140,BPM_PAIR=08-01")

#######################################################
# Section DB - S1: MEBT-010
dbLoadRecords("$(bpmmanager_DB)/bpmmanager_section.db","P=$(P),S=$(S1),Ss=$(S1s)")


############################################################################
iocInit()

