BPM Manager
======
IOC application that works as a top-level IOC for the BPM system. It provides three types of PVs that can perform actions on groups of PVs from different IOCs:
- Fanout group: PV that forwards its value to a list of PVs that live in different IOCs;
- Status group: PV that monitors the (binary) status of a list of PVs from different IOCs and applies an AND logic function;
- Waveform group: Waveform PV in which each of its array elements is a scalar value (PV) from a different IOC;

Using the module 
================ 
This module is used as a plain EPICS IOC application. The configuration of the group PVs that it will generate should be given by DB files;

Phoebus OPI
================
OPI related to this module can be found here:
https://gitlab.esss.lu.se/icshwi/bpmmanager-opi 
